<?php
define('CONTACTUS_SETTINGCATEGORY','Contact Us');

define('CONTACTUS_SETTING_ADDRESS','Address');
define('CONTACTUS_SETTING_ADDRESS_DESC','Main address displayed in contact infos blocks.');
define('CONTACTUS_SETTING_PHONE','Main phone number');
define('CONTACTUS_SETTING_PHONE_DESC','This phone number will be displayed in all contact infos blocks added by the contact plugin.');
define('CONTACTUS_SETTING_FAX','Main fax number');
define('CONTACTUS_SETTING_FAX_DESC','This fax number will be displayed in all contact infos blocks added by the contact plugin.');
define('CONTACTUS_SETTING_EMAIL','Main email address');
define('CONTACTUS_SETTING_EMAIL_DESC','This email address will be displayed in contact infos blocks all over the site.');
?>