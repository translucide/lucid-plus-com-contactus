<?php
define('CONTACTUS_SETTINGCATEGORY','Contactez-nous');

define('CONTACTUS_CONTENT_TYPE','Contactez-nous');
define('CONTACTUS_CONTENT_TYPE_DESC','Ajoute un formulaire de contact configurable');

define('CONTACTUS_CONTENT_DISPLAYSUBJECT','Demander le sujet?');
define('CONTACTUS_CONTENT_DISPLAYBODY','Demander le message?');
define('CONTACTUS_CONTENT_DISPLAYNAME','Demander le nom?');
define('CONTACTUS_CONTENT_DISPLAYCIE','Demander le nom de la compagnie?');
define('CONTACTUS_CONTENT_DISPLAYADDR','Demander une adresse?');
define('CONTACTUS_CONTENT_DISPLAYCITY','Demander la ville?');
define('CONTACTUS_CONTENT_DISPLAYPCODE','Demander le code postal?');
define('CONTACTUS_CONTENT_DISPLAYPHONE','Demander le numéro de téléphone?');
define('CONTACTUS_CONTENT_DISPLAYEMAIL','Demander une adresse courriel?');
define('CONTACTUS_CONTENT_DISPLAYFAX','Demander un numéro de fax?');
define('CONTACTUS_CONTENT_DISPLAYAGE','Demander l\'âge?');
define('CONTACTUS_CONTENT_DISPLAYSERVICES','Demander les services qui intéressent le client?');
define('CONTACTUS_CONTENT_SERVICES','Liste des services, un par ligne');

define('CONTACTUS_TITLE','Contactez-nous');
define('CONTACTUS_INTRO','Veuillez remplir ce formulaire afin de nous joindre. Nous vous répondrons sous peu.');
define('CONTACTUS_NAME','Nom');
define('CONTACTUS_CIE','Entreprise');
define('CONTACTUS_ADDRESS','Adresse');
define('CONTACTUS_CITY','Ville');
define('CONTACTUS_PCODE','Code postal');
define('CONTACTUS_EMAIL','Adresse courriel');
define('CONTACTUS_FAX','Fax');
define('CONTACTUS_AGE','Âge');
define('CONTACTUS_SERVICES','Services');
define('CONTACTUS_PHONENUMBER','Numéro de téléphone');
define('CONTACTUS_PHONE','Numéro de téléphone');
define('CONTACTUS_SUBJECT','Sujet');
define('CONTACTUS_BODY','Message');
define('CONTACTUS_SEND','Envoyer');
define('CONTACTUS_SENDCONFIRM','Votre message à été envoyé avec succès. Nous vous reviendrons sous peu.');
define('CONTACTUS_SENDFAILED','Votre message n\'a pu être envoyé. Veuillez réessayer plus tard.');
define('CONTACTUS_NOTVALID','Veuillez remplir tout les champs !');
define('CONTACTUS_CLIENTIP','Envoyé depuis cette adresse IP');
define('CONTACTUS_CLIENTLANG','Langue utilisée par le client');


define('CONTACTUS_SETTING_PHONE','Numéro de téléphone principal');
define('CONTACTUS_SETTING_PHONE_DESC','Ce numéro sera affiché dans tous les blocs fournis par le plugin de contact.');
define('CONTACTUS_SETTING_EMAIL','Adresse courriel principale');
define('CONTACTUS_SETTING_EMAIL_DESC','Cette adresse sera affichée à travers le site.');

define('CONTACTUS_WIDGET_TITLE','Bloc Contactez-Nous');
define('CONTACTUS_WIDGET_DESC',' Vous permet d\'afficher les informations de contact de la manière qui vous convient sur votre site web.');
define('CONTACTUS_WIDGET_TYPE','Choisissez le type de bloc');
define('CONTACTUS_WIDGET_TYPE_1','Afficher le numéro de téléphone seulement');
define('CONTACTUS_WIDGET_TYPE_2','Afficher le numéro de téléphone et l\'adresse courriel principale uniquement');
define('CONTACTUS_WIDGET_TYPE_3','Afficher le numéro de téléphone, le courriel et un bouton "contactez-nous".');
define('CONTACTUS_WIDGET_BUTTONTEXT','Button text (if applicable)');

define('CONTACTUS_FORMWIDGET_TITLE','Formulaire');
define('CONTACTUS_FORMWIDGET_TITLEDESC','Ajoute un formulaire à votre site web.');
define('CONTACTUS_FORMWIDGET_FIELDS','Champs');
define('CONTACTUS_FORMWIDGET_DESTINATION','Adresse courriel à laquelle envoyer le formulaire');
define('CONTACTUS_FORMWIDGET_CONFIRMMSG','Message de confirmation de l\'envoi');
define('CONTACTUS_FORMWIDGET_SENDCOPY','Envoyer une copie du message au client?');
define('CONTACTUS_FORMWIDGET_LAYOUT','Disposition des éléments');
define('CONTACTUS_FORMWIDGET_LAYOUT_H','Horizontal');
define('CONTACTUS_FORMWIDGET_LAYOUT_V','Vertical');
define('CONTACTUS_FORMWIDGET_SUBJECT','Sujet du mail');
define('CONTACTUS_FORMWIDGET_TYPE','Type de champ');
define('CONTACTUS_FORMWIDGET_FIELD_TEXT','Texte sur une ligne');
define('CONTACTUS_FORMWIDGET_FIELD_CHECKBOX','Case à cocher');
define('CONTACTUS_FORMWIDGET_FIELD_DATE','Date');
define('CONTACTUS_FORMWIDGET_FIELD_DIMENSIONS','Dimensions (largeur et hauteur)');
define('CONTACTUS_FORMWIDGET_FIELD_FILE','Fichier');
define('CONTACTUS_FORMWIDGET_FIELD_IMAGE','Image');
define('CONTACTUS_FORMWIDGET_FIELD_HTMLEDITOR','Éditeur HTML');
define('CONTACTUS_FORMWIDGET_FIELD_LABEL','Label');
define('CONTACTUS_FORMWIDGET_FIELD_PWD','Mot de passe');
define('CONTACTUS_FORMWIDGET_FIELD_RADIO','Bulles à cocher');
define('CONTACTUS_FORMWIDGET_FIELD_SELECT','Sélecteur');
define('CONTACTUS_FORMWIDGET_FIELD_LANGSELECT','Sélecteur de langue');
define('CONTACTUS_FORMWIDGET_FIELD_MULTISELECT','Sélection de choix multiples');
define('CONTACTUS_FORMWIDGET_FIELD_GROUPSELECT','Sélecteur de groupes d\'usagers');
define('CONTACTUS_FORMWIDGET_FIELD_SUBMITBTN','Bouton soumettre');
define('CONTACTUS_FORMWIDGET_FIELD_TEXTAREA','Texte, plusieurs lignes');
define('CONTACTUS_FORMWIDGET_FIELD_YESNO','Oui/Non');
define('CONTACTUS_FORMWIDGET_SHOWHELP','Afficher l\'aide lors de la sélection des options');
define('CONTACTUS_FORMWIDGET_ISSOURCEEMAIL','Ce champ représente l\'adresse courriel à laquelle envoyer une copie du message');
define('CONTACTUS_FORMWIDGET_ISSUBJECT','Ce champ représente le sujet du courriel');
define('CONTACTUS_FORMWIDGET_OPT','Options');
define('CONTACTUS_FORMWIDGET_OPTDESC','Une option par ligne en respectant les formats requis: valeur|titre|help ou valeur|titre ou titre seulement.');
define('CONTACTUS_FORMWIDGET_HELP','Texte d\'aide du champ');
define('CONTACTUS_FORMWIDGET_SAVE','Enregistrer le champ');
define('CONTACTUS_FORMWIDGET_DWN','Télécharger le ficher');
define('CONTACTUS_FORMWIDGET_FROMIP','Envoyé depuis cette adresse IP: ');
define('CONTACTUS_FORMWIDGET_FROMLANG','Langue utilisée à l\'envoi: ');
define('CONTACTUS_FORMWIDGET_WHY','Pourquoi ai-je reçu ce courriel ?');
define('CONTACTUS_FORMWIDGET_WHYTEXT','Vous recevez ce courriel car un usager à rempli le formulaire intitulé "{title}" depuis le site de {site}. Si vous n\'êtes pas l\'auteur de cette action, veuillez communiquer avec nous à {email}.');
define('CONTACTUS_FORMWIDGET_INTROTEXT','Voici les informations entrées dans le formulaire {title} depuis le site {site}');
?>