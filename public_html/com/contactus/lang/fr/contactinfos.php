<?php
define('CONTACTUS_WIDGET_CONTACTINFOS_TITLE','Informations de contact');
define('CONTACTUS_WIDGET_CONTACTINFOS_DESC','Affiche vos informations de contact.');
define('CONTACTUS_WIDGET_CONTACTINFOS_INTRO','Introduction');
define('CONTACTUS_WIDGET_CONTACTINFOS_LINK','URL de la page de contact');
define('CONTACTUS_WIDGET_CONTACTINFOS_LINKTEXT','Titre du bouton "Contactez-nous"');
define('CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYEMAIL','Afficher l\'adresse courriel?');
define('CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYFAX','Afficher le numéro de fax?');
define('CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYADDRESS','Afficher l\'adresse?');
define('CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYPHONE','Afficher le numéro de téléphone?');
define('CONTACTUS_WIDGET_CONTACTINFOS_EMAIL','Courriel');
define('CONTACTUS_WIDGET_CONTACTINFOS_FAX','Fax');
define('CONTACTUS_WIDGET_CONTACTINFOS_ADDRESS','Adresse');
define('CONTACTUS_WIDGET_CONTACTINFOS_PHONE','Téléphone');