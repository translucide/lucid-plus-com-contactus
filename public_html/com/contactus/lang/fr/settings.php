<?php
define('CONTACTUS_SETTINGCATEGORY','Coordonnées');

define('CONTACTUS_SETTING_ADDRESS','Adresse');
define('CONTACTUS_SETTING_ADDRESS_DESC','Adresse principale affichée dans les blocs de contact.');
define('CONTACTUS_SETTING_PHONE','Numéro de téléphone principal');
define('CONTACTUS_SETTING_PHONE_DESC','Ce numéro de téléphone sera affiché dans tous les blocs de contact ajoutés par le plugin.');
define('CONTACTUS_SETTING_FAX','Numéro de fax principal');
define('CONTACTUS_SETTING_FAX_DESC','Le numéro de fax sera affiché dans tous les blocs de contact.');
define('CONTACTUS_SETTING_EMAIL','Adresse courriel principale');
define('CONTACTUS_SETTING_EMAIL_DESC','Cette adresse courriel sera affichée dans les blocs d\'informations de contact.');
?>