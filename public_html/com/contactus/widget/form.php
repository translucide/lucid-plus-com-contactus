<?php
/**
 * Form by mail widget handler
 *
 * Handles form content type
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('com/contactus/lang/'.$service->get('Language')->getCode().'/contactus');
$service->get('Ressource')->get('core/helper/dbitemmanager');

class FormWidget extends Widget{

	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'contactus',
			'type' => 'block',
			'name' => 'form',
			'title' => CONTACTUS_FORMWIDGET_TITLE,
			'description' => CONTACTUS_FORMWIDGET_TITLEDESC,
			'icon' => 'contactus',
			'wireframe' => 'form',
			'saveoptions' => array(
				'email','confirmmsg','sendcopytoclient','subject','exerpt','content','layout'
			),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	/**
	 * Returns the edit form to modify a section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);

		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);
		$form->add(new HtmleditorFormField('exerpt',$defobj->getVar('widget_options')['exerpt'],array(
			'title'=>_HEADER,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'widget_options','exerpt')
		)));

		//Form creation code...
		$form->add(new ItemmanagerFormField('fields',$defobj->getVar('widget_options')['fields'],array(
			'title'=>CONTACTUS_FORMWIDGET_FIELDS,
			'tab'=>'basic',
			'parent' => $defobj->getVar('widget_objid'),
			'editlabel' => _EDIT,
			'deletelabel' => _DELETE,
			'duplicatelabel' => _DUPLICATE,
			'newlabel' => _NEW,
			'list' => URL.$service->get('Language')->getCode().'/api/contactus/widget/form/list',
			'edit' => URL.$service->get('Language')->getCode().'/api/contactus/widget/form/edit',
			'duplicate' => URL.$service->get('Language')->getCode().'/api/contactus/widget/form/duplicate',
			'save' => URL.$service->get('Language')->getCode().'/api/contactus/widget/form/save',
			'delete' => URL.$service->get('Language')->getCode().'/api/contactus/widget/form/delete',
			'move' => URL.$service->get('Language')->getCode().'/api/contactus/widget/form/move',
		)));
		$form->add(new TextFormField('email',$defobj->getVar('widget_options')['email'],array(
			'title'=>CONTACTUS_FORMWIDGET_DESTINATION,
			'length'=>100,
			'tab'=>'basic'
		)));
		$form->add(new HtmleditorFormField('confirmmsg',$defobj->getVar('widget_options')['confirmmsg'],array(
			'title'=>CONTACTUS_FORMWIDGET_CONFIRMMSG,
			'length'=>100,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'widget_options','confirmmsg')
		)));
		$form->add(new YesnoFormField('sendcopytoclient',$defobj->getVar('widget_options')['sendcopytoclient'],array(
			'title'=>CONTACTUS_FORMWIDGET_SENDCOPY,
			'length'=>1,
			'tab'=>'basic',
			'width' => 6
		)));
		$form->add(new SelectFormField('layout',$defobj->getVar('widget_options')['layout'],array(
			'title'=>CONTACTUS_FORMWIDGET_LAYOUT,
			'width' => 6,
			'tab'=>'basic',
			'options' => array(
				array('title' => CONTACTUS_FORMWIDGET_LAYOUT_H, 'value' => 'horizontal'),
				array('title' => CONTACTUS_FORMWIDGET_LAYOUT_V, 'value' => 'vertical')
			)
		)));
		$form->add(new TextFormField('subject',$defobj->getVar('widget_options')['subject'],array(
			'title'=>CONTACTUS_FORMWIDGET_SUBJECT,
			'length'=>100,
			'tab'=>'basic'
		)));
		return $form;
	}

	/**
	 * Deletes content from other tables than section & content tables
	 */
	public function delete(){

	}

	/**
	 * Renders the section visual elements
	 */
	public function render(){
		global $service;
		$service->get('Ressource')->get('core/display/form');
		$service->get('Ressource')->get('lib/packages/form');
		$service->get('Ressource')->addScript('$(document).ready(function(){
			$.post("'.URL.$service->get('Language')->getCode().'/api/contactus/widget/form/render",{"id":"'.$this->data->getVar('widget_objid').'"},function(data){
				if (data.success){ $("#form_widget_'.$this->data->getVar('widget_objid').'_launcher").html(data.data);}
			});});'
		);
		$code = '<div id="form_widget_'.$this->data->getVar('widget_objid').'_div"></div>';
		$code .= '<div id="form_widget_'.$this->data->getVar('widget_objid').'_launcher"></div>';
/*		$js = '<script src="'.URL.'lib/vendor/ajaxform.js" language="javascript" type="text/javascript"></script>'."\n".
        '<script src="'.URL.'js.js?file[]=sys%2Fcore%2Fdisplay%2Fform%2Fform.js" language="javascript" type="text/javascript"></script>'."\n".
        '<script language="javascript" type="text/javascript">$(document).ready(function(){$.post("'.URL.'en/api/contactus/widget/form/render",{"id":"'.$this->data->getVar('widget_objid').'"},function(data){if (data.success){ $("#form_widget_'.$this->data->getVar('widget_objid').'_launcher").html(data.data);}});});</script>';*/
		return $code;
	}

	public function getRessources(){
		global $service;
		$service->get('Ressource')->get('core/display/form');
		$service->get('Ressource')->get('lib/vendor/ajaxform');
        $service->get('Ressource')->get('lib/packages/form');
	}

	public function apiCall($op){
		global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();
		$DBItemManager = new DBItemManager($store,'formitem');
		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' : {
				$ret = $DBItemManager->$op();
			}break;
			case 'duplicate' :
			case 'edit' : {
				$ret = '';
				if ($op == 'duplicate') $ret = $DBItemManager->duplicate();
				$data = $DBItemManager->getEditObjects($ret);
				$obj = $data['data'];
				$form = $data['form'];
				$defobj = $store->getDefaultObj($obj);
				$form->add(new HiddenFormfield('data_type','formitem'));
				$form->add(new TextFormField('fieldname',$defobj->getVar('data_data')['fieldname'],array(
					'title'=>_NAME,
					'width' => 4,
					'length'=>255
				)));
				$form->add(new SelectFormField('fieldtype',$defobj->getVar('data_data')['fieldtype'],array(
					'title'=>CONTACTUS_FORMWIDGET_TYPE,
					'width' => 4,
					'options' => array(
						array('title' => CONTACTUS_FORMWIDGET_FIELD_TEXT, 'value' => 'text'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_CHECKBOX, 'value' => 'checkbox'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_DATE, 'value' => 'datepicker'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_DIMENSIONS, 'value' => 'imagesize'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_FILE, 'value' => 'fileupload'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_IMAGE, 'value' => 'imageupload'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_HTMLEDITOR, 'value' => 'htmleditor'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_LABEL, 'value' => 'label'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_PWD, 'value' => 'password'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_RADIO, 'value' => 'radio'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_SELECT, 'value' => 'select'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_LANGSELECT, 'value' => 'selectlanguage'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_MULTISELECT, 'value' => 'selectmultiple'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_GROUPSELECT, 'value' => 'selectusergroup'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_SUBMITBTN, 'value' => 'submit'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_TEXTAREA, 'value' => 'textarea'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_YESNO, 'value' => 'yesno'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_HIDDEN, 'value' => 'hidden')
					)
				)));
				$form->add(new TextFormField('placeholder',$defobj->getVar('data_data')['placeholder'],array(
					'title'=>'Texte indicatif (placeholder)',
					'width' => 4,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','placeholder')
				)));
				$form->add(new TextFormField('value',$defobj->getVar('data_data')['value'],array(
					'title'=>'Valeur par défaut',
					'width' => 4,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','value')
				)));
				$form->add(new SelectcellwidthFormField('fieldwidth',$defobj->getVar('data_data')['fieldwidth'],array(
					'title'=>_WIDTH,
					'width' => 4
				)));
				$form->add(new SelectFormField('validationtype',$defobj->getVar('data_data')['validationtype'],array(
					'title'=>_VALIDATE,
					'width' => 4,
					'length'=>255,
					'options' => array(
						array('title' => _CHOOSE, 'value' => ''),
						array('title' => _NUMBER, 'value' => 'number'),
						array('title' => _PHONENUMBER, 'value' => 'phonenumber'),
						array('title' => _EMAIL, 'value' => 'email')
					)
				)));
				$form->add(new TextFormField('redirecturl',$defobj->getVar('data_data')['redirecturl'],array(
					'title'=>'Submit button Redirect URL',
					'width' => 4,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','redirecturl')
				)));
				$form->add(new YesnoFormField('showhelponclick',$defobj->getVar('data_data')['showhelponclick'],array(
					'title'=>CONTACTUS_FORMWIDGET_SHOWHELP,
					'width' => 4,
					'length'=>255,
				)));
				$form->add(new YesnoFormField('issourceemail',$defobj->getVar('data_data')['issourceemail'],array(
					'width' => 4,
					'title'=>CONTACTUS_FORMWIDGET_ISSOURCEEMAIL
				)));
				$form->add(new YesnoFormField('issubject',$defobj->getVar('data_data')['issubject'],array(
					'width' => 4,
					'title'=>CONTACTUS_FORMWIDGET_ISSUBJECT
				)));
				$form->add(new YesnoFormField('requiredfield',$defobj->getVar('data_data')['requiredfield'],array(
					'title'=>_REQUIRED,
					'width' => 6
				)));
				$form->add(new YesnoFormField('disabledfield',$defobj->getVar('data_data')['disabledfield'],array(
					'title'=>_DISABLED,
					'width' => 6
				)));
				$form->add(new TextareaFormField('options',$defobj->getVar('data_data')['options'],array(
					'title'=>CONTACTUS_FORMWIDGET_OPT,
					'width' => 6,
					'length'=>255,
					'help' => CONTACTUS_FORMWIDGET_OPTDESC,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','options')
				)));
				$form->add(new TextareaFormField('help',$defobj->getVar('data_data')['help'],array(
					'title'=>CONTACTUS_FORMWIDGET_HELP,
					'width' => 6,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','help')
				)));

				$ret = $DBItemManager->edit($form);
			}break;
			case 'save' : {
				$ret = $DBItemManager->save(array('fieldname','fieldtype','placeholder','value', 'fieldwidth', 'options', 'requiredfield','disabledfield','showhelponclick', 'help', 'validationtype', 'issourceemail','issubject','redirecturl'));
			}break;
			case 'render':{
				global $service;
				$service->get('Ressource')->get('core/data');
				$service->get('Ressource')->get('core/display/form');
				$service->get('Ressource')->get('core/display/form/field');
				$request = $service->get('Request')->get();
				$wstore = new WidgetStore();
				$wstore->setOption('ignorelangs',false);
				$store->setOption('ignorelangs',false);
				$obj = $wstore->getByObjId($request['id'])[0];
				if (!is_object($obj)) return array('success' => false);

				// 1. Load all form elements
				$crit = new CriteriaCompo(new Criteria('data_type','formitem'));
				$crit->add(new Criteria('data_parent',$request['id']));
				$crit->setSort('data_position');
				$fields = $store->get($crit);

				//2. Setup form...
				$form = new Form(URL.$service->get('Language')->getCode().'/api/contactus/widget/form/contactusformwidgetsend','form_widget_'.$obj->getVar('widget_objid'),'','POST');
				$form->setUseCase('ajax');
				$form->setVar('addscripttags',false);
				$form->setVar('layout',$obj->getVar('widget_options')['layout']);
				$form->setVar('formtag',true);
				$form->setVar('intro',$obj->getVar('widget_options')['exerpt']);
				$form->add(new HiddenFormField('op','contactusformwidgetsend'));
				$form->add(new HiddenFormField('id',$obj->getVar('widget_objid')));

				//3. Add form fields to the form...
				$redirects = "";
				foreach ($fields as $k => $v) {
					$opt = $v->getVar('data_data');

					//Parse options...
					if (trim($opt['options']) != '') {
						$options = array();
						$opt['options'] = explode("\n",$opt['options']);
						foreach ($opt['options'] as $ko => $vo) {
							$tmpo = explode('|',$vo);
							switch (count($tmpo)) {
								case '1' : $options[] = array('title' => $tmpo[0], 'value' => trim($tmpo[0])); break;
								case '2' : $options[] = array('title' => $tmpo[0], 'value' => trim($tmpo[1])); break;
								case '3' : $options[] = array('title' => $tmpo[0], 'value' => trim($tmpo[1]), 'help' => $tmpo[2]); break;
							}
						}
					}
					$cls = ucfirst(strtolower($opt['fieldtype'])).'FormField';
					if ($opt['requiredfield']) {
						$validate = array(
							array('method'=>'notempty')
						);
						if ($opt['validationtype'] != '') $validate[] = array('method' => $opt['validationtype']);
					}
					else $validate = array();
					$name = 'formfield'.$k;
					if (isset($opt['issubject']) && $opt['issubject']) $name = 'subject';
					if (isset($opt['fieldname']) && strlen($opt['fieldname']) > 0) $name = $opt['fieldname'];

					//Set proper redirects per submit button
					if (trim($opt['redirecturl']) != '') {
						$redirects .= '$("#form_widget_'.$obj->getVar('widget_objid').'_div button[name='.$name.']").on("click",function(){
							$("#form_widget_'.$obj->getVar('widget_objid').'_div").on("submitSuccess",function(){
								window.location = "'.trim($opt['redirecturl']).'";
							});
						});';
					}else {
						$redirects .= '$("#form_widget_'.$obj->getVar('widget_objid').'_div button[name='.$name.']").on("click",function(){
							$("#form_widget_'.$obj->getVar('widget_objid').'_div").on("submitSuccess",function(){
								return false;
							});
						});';
                    }
					$form->add(new $cls($name,trim($opt['value']),array(
						'title' => $v->getVar('data_title'),
						'placeholder' => trim($opt['placeholder']),
						'options' => $options,
						'showhelponclick' => $opt['showhelponclick'],
						'help' => $opt['help'],
						'disabled' => $opt['disabledfield'],
						'width' => $opt['fieldwidth'],
						'validate' => $validate
					)));
				}
				$ret = array('success' => true,'data'=> '<script async="false" language="javascript" type="text/javascript">'.$form->render().
					'setTimeout(function(){$(\'#form_widget_'.$obj->getVar('widget_objid').'_div form\').ajaxForm({
						success: function(responseText, statusText, xhr, $form){
							if (responseText.success) {
								$("#form_widget_'.$obj->getVar('widget_objid').'_div").hide().html("<div class=\"alert alert-success\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a> "+responseText.message+"</div>").fadeIn();
								$("#form_widget_'.$obj->getVar('widget_objid').'_div").trigger("submitSuccess");
							}else {
								$("#form_widget_'.$obj->getVar('widget_objid').'_div").parent().children(".alert-error").remove();
								$("#form_widget_'.$obj->getVar('widget_objid').'_div").parent().append("<div class=\"alert alert-error\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a> "+responseText.message+"</div>");
								$("#form_widget_'.$obj->getVar('widget_objid').'_div").trigger("submitError");
							}
						}
					});
					'.$redirects.'

					},1000);</script>');
			}break;
			case 'contactusformwidgetsend' : {
				global $service;
				$service->get('Ressource')->get('core/data');
				$request = $service->get('Request')->get();
				$wstore = new WidgetStore();
				$store = new DataStore();
				$wstore->setOption('ignorelangs',false);
				$store->setOption('ignorelangs',false);
				$obj = $wstore->getByObjId($request['id']);
				$obj = $obj[0];

				// 1. Load all form elements
				$crit = new CriteriaCompo(new Criteria('data_type','formitem'));
				$crit->add(new Criteria('data_parent',$request['id']));
				$crit->setSort('data_position');
				$fields = $store->get($crit);

				$body = str_replace("{title}",$obj->getVar('widget_title'),str_replace("{site}",$service->get('Setting')->get('sitename'),CONTACTUS_FORMWIDGET_INTROTEXT))."\n\n\n";
				$sourceemail = '';
				$subject = '';

				//3. Get form fields values from the form...
				foreach ($fields as $k => $v){
					$opt = $v->getVar('data_data');
					$name = 'formfield'.$k;
					if (isset($opt['issubject']) && $opt['issubject']) $name = 'subject';
					if (isset($opt['fieldname']) && strlen($opt['fieldname']) > 0) $name = $opt['fieldname'];

					if ($opt['fieldtype'] != 'button' && $opt['fieldtype'] != 'submit') {
						if ($opt['fieldtype'] == 'textarea' || $opt['fieldtype'] == 'htmleditor') $body .= "\n";
						$body .= $v->getVar('data_title').': ';
					}

					if ($opt['issourceemail']) $sourceemail = $request[$name];
					if ($opt['issubject']) $subject = $request[$name];
					if ($opt['fieldtype'] == 'textarea' || $opt['fieldtype'] == 'htmleditor') {
						$body .= "\n";
					}
					//Parse options...
					if (trim($opt['options']) != '' && is_array($request[$name])) {
						$body .= str_replace("\n\n","\n","\n&#10004;".implode("\n&#10004; ",$request[$name])."\n");
					}else {
						if ($opt['fieldtype'] == 'fileupload' || $opt['fieldtype'] == 'imageupload') {
							$body .= '<a href="'.URL.$request[$name].'">'.CONTACTUS_FORMWIDGET_DWN.'</a>';
						}
						else {
							if ($opt['issubject']) $body.= $subject."\n";
							else {
								if ($opt['fieldtype'] == 'yesno') $body .= (($request[$name] == 1)?_YES:_NO)."\n";
								else $body .= $request[$name]."\n";
							}
						}
					}
					if ($opt['fieldtype'] == 'textarea' || $opt['fieldtype'] == 'htmleditor') {
						$body .= "\n";
					}
				}
				$ip = "\n\n<br><br>".CONTACTUS_FORMWIDGET_FROMIP.'<a href="http://whatismyipaddress.com/ip/'.$_SERVER['REMOTE_ADDR'].'">'.$_SERVER['REMOTE_ADDR']."</a>\n";
				$body .= CONTACTUS_FORMWIDGET_FROMLANG.strtoupper($service->get('Language')->getCode())."\n";

				$service->get('Ressource')->get('core/protocol/email');
				$email = new Email();
				$email->html(true);

				$to = $obj->getVar('widget_options')['email'];
				if ($to != '') $email->to($to);
				$email->to($service->get('Setting')->get('adminmail'));

				if ($obj->getVar('widget_options')['subject'] != '') $subject = $obj->getVar('widget_options')['subject'].' ('.$subject.')';
				$email->subject($subject);

				$theme = $service->get('Ressource')->getTheme();
				$tpl = $theme->getEmailTemplate();
				$body = $body.$ip;
				$tpl->assign('content',nl2br($body));
				$tpl->assign('title',$subject);
				$email->body($tpl->render());
				$email->from($sourceemail,$sourceemail);
				$res = $email->queue();

				if ($res == false) {
					$res = mail($to,$subject,$body,'from:'.$sourcemail);
					mail($service->get('Setting')->get('adminmail'),'Form send by mail failed',$email->error()."\n\nOriginal message: ".$body);
				}
				//Send a copy to the client if checked....
				if ($obj->getVar('widget_options')['sendcopytoclient'] == 1 && $sourceemail != '') {
					$body .= "\n\n\n<b>".CONTACTUS_FORMWIDGET_WHY."</b>\n\n";
					$body .= str_replace("{title}",$obj->getVar('widget_title'),
						str_replace("{site}",$service->get('Setting')->get('sitename'),
						str_replace("{email}",$obj->getVar('widget_options')['email'],CONTACTUS_FORMWIDGET_WHYTEXT)));
					$tpl->assign('content',nl2br($body));
					$email = new Email();
					$email->html(true);
					$email->to($sourceemail);
					$email->subject($obj->getVar('widget_title'));
					$email->from($obj->getVar('widget_options')['email'],$obj->getVar('widget_options')['email']);
					$email->body($tpl->render());
					$email->queue();
				}
                $confirmmsg = $obj->getVar('widget_options')['confirmmsg'];
                if ($confirmmsg == '<p></p>' || $confirmmsg == '<p>&nbsp;</p>') $confirmmsg = '';
				if ($res) $ret = array('success' => true, 'message' => ($confirmmsg)?$confirmmsg:CONTACTUS_SENDCONFIRM);
				else $ret = array('success' => false, 'message' => CONTACTUS_SENDFAILED);
			}break;
			default : break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'contactusformwidgetsend' :
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}
}
