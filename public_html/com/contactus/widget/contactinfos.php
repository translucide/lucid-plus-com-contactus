<?php
/**
 * Contact Us widget
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/contactus/lang/'.$service->get('Language')->getCode().'/contactinfos');

class ContactinfosWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'contactus',
			'type' => 'block',
			'name' => 'contactinfos',
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_TITLE,
			'description' => CONTACTUS_WIDGET_CONTACTINFOS_DESC,
			'wireframe' => 'contactinfos',
			'icon' => 'contactinfos',
			'saveoptions' => array('intro','displayphone','displayfax','displayemail','displayaddr','buttontitle','contactuslink')
		));
	}

	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('contactus','widget','contactinfos',$opt['stylesheet']);

		$url = str_replace(['https:'.URL,'http:'.URL,URL],'',$opt['contactuslink']);
        $url = URL.$url;
        $content = '
		<section class="widget html contactinfos '.$opt['stylesheet'].' '.$o['widgetclasses'].'">
	        <div class="ct">
				<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1>';
		if ($opt['intro']) $content .= '<p class="intro">'.$opt['intro'].'</p>';
		if ($opt['displayaddr']) $content .= '<p class="address"><b>'.CONTACTUS_WIDGET_CONTACTINFOS_ADDRESS.':</b>'.nl2br($service->get('Setting')->get('contactus_address')).'</p>';
		if ($opt['displayphone']) $content .= '<p class="phone"><b>'.CONTACTUS_WIDGET_CONTACTINFOS_PHONE.':</b>'.$service->get('Setting')->get('contactus_phone').'</p>';
		if ($opt['displayemail']) $content .= '<p class="email"><b>'.CONTACTUS_WIDGET_CONTACTINFOS_EMAIL.':</b>'.str_replace('@','[@]',$service->get('Setting')->get('contactus_email')).'</p>';
		if ($opt['displayfax']) $content .= '<p class="fax"><b>'.CONTACTUS_WIDGET_CONTACTINFOS_FAX.':</b>'.$service->get('Setting')->get('contactus_fax').'</p>';
		$content .= '<a href="'.$url.'" class="btn btn-primary">'.$opt['buttontitle'].'</a>
			</div>
		</section>';
		return $content;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

		$form->add(new TextareaFormField('intro',$options['intro'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_INTRO,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','intro')
		)));
		$form->add(new TextFormField('contactuslink',$options['contactuslink'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_LINK,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','contactuslink')
		)));
		$form->add(new TextFormField('buttontitle',$options['buttontitle'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_LINKTEXT,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','buttontitle')
		)));
		$form->add(new YesnoFormField('displayphone',$options['displayphone'],array(
			'tab'=> 'basic',
			'width' => 3,
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYPHONE,
		)));
		$form->add(new YesnoFormField('displayemail',$options['displayemail'],array(
			'tab'=> 'basic',
			'width' => 3,
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYEMAIL,
		)));
		$form->add(new YesnoFormField('displayfax',$options['displayfax'],array(
			'tab'=> 'basic',
			'width' => 3,
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYFAX,
		)));
		$form->add(new YesnoFormField('displayaddr',$options['displayaddr'],array(
			'tab'=> 'basic',
			'width' => 3,
			'title' => CONTACTUS_WIDGET_CONTACTINFOS_DISPLAYADDRESS,
		)));

		return $form;
	}
}
?>
