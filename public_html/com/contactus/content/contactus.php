<?php
/**
 * Contactus content handler
 *
 * Handles contactus content type
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Content');
$service->get('Ressource')->get('core/display/converter/requesttoobject');
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('com/contactus/lang/'.$service->get('Language')->getCode().'/contactus');

class ContactusContent extends Content{
	
	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'contactus',
			'type' => 'contactus',
			'title' => CONTACTUS_CONTENT_TYPE,
			'description' => CONTACTUS_CONTENT_TYPE_DESC,
			'icon' => 'contactus',
			'saveoptions' => array(
				'displaysubject','displaybody','displayname','displaycie',
				'displayaddr','displaycity','displaypcode','displayphone',
				'displayemail','displayfax','displayage','displayservices','services'
			)
		));
	}

	/**
	 * Returns the edit form to modify a section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		
		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);
		$options = $defobj->getVar('content_options');
		
		$form->setVar('title',_EDIT.' '.$defobj->getVar('content_title'));
		$form->add(new LucideditorFormField('content_content',$defobj->getVar('content_content'),array(
			'title'=>_INTRO,
			'length'=>100,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'content_content')
		)));
		$form->add(new YesnoFormField('displaysubject',$options['displaysubject'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYSUBJECT,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displaybody',$options['displaybody'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYBODY,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displayname',$options['displayname'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYNAME,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displaycie',$options['displaycie'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYCIE,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displayaddr',$options['displayaddr'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYADDR,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displaycity',$options['displaycity'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYCITY,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displaypcode',$options['displaypcode'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYPCODE,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displayphone',$options['displayphone'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYPHONE,
			'width' => 2,
		)));
		$form->add(new YesnoFormField('displayemail',$options['displayemail'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYEMAIL,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displayfax',$options['displayfax'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYFAX,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displayage',$options['displayage'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYAGE,
			'width' => 2,
		)));		
		$form->add(new YesnoFormField('displayservices',$options['displayservices'],array(
			'tab'=> 'basic',
			'title' => CONTACTUS_CONTENT_DISPLAYSERVICES,
			'width' => 2,
		)));
		$form->add(new TextareaFormField('services',$options['services'],array(
			'title'=>CONTACTUS_CONTENT_SERVICES,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'content_options','services')
		)));		
		return $form;
	}
	
	/**
	 * Renders the section visual elements
	 */
	public function render($asExerpt=false){
		if ($asExerpt) return '';
		global $service;
		$request = $service->get('Request')->get();
		$options = $this->data->getVar('content_options');
		$url = $service->get('Url')->get();
		
		$services = array();
		$tmp = explode("\n",$options['services']);
		foreach ($tmp as $k => $v) {
			$services[] = array('title' => $v, 'value' => $v);
		}
		//Build an array of all fields we have to manage...
		$fields = array(
			'name' => array('name' => 'name', 'title' => CONTACTUS_NAME, 'on' => $options['displayname'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'cie' => array('name' => 'cie', 'title' => CONTACTUS_CIE, 'on' => $options['displaycie'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'addr' => array('name' => 'addr', 'title' => CONTACTUS_ADDRESS, 'on' => $options['displayaddr'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'city' => array('name' => 'city', 'title' => CONTACTUS_CITY, 'on' => $options['displaycity'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'pcode' => array('name' => 'pcode', 'title' => CONTACTUS_PCODE, 'on' => $options['displaypcode'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'phone' => array('name' => 'phone', 'title' => CONTACTUS_PHONE, 'on' => $options['displayphone'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'email' => array('name' => 'email', 'title' => CONTACTUS_EMAIL, 'on' => $options['displayemail'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'fax' => array('name' => 'fax', 'title' => CONTACTUS_FAX, 'on' => $options['displayfax'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'age' => array('name' => 'age', 'title' => CONTACTUS_AGE, 'on' => $options['displayage'],'type'=>'Text','width'=>4,'validate' => [['method' => 'notempty']]),
			'services' => array('name' => 'services', 'title' => CONTACTUS_SERVICES, 'on' => $options['displayservices'],'options'=>$services,'type'=>'Checkbox','width'=>12,'validate' => [['method' => 'notempty']]),
			'subject' => array('name' => 'subject', 'title' => CONTACTUS_SUBJECT, 'on' => $options['displaysubject'],'type'=>'Text','width'=>12,'validate' => [['method' => 'notempty']]),
			'body' => array('name' => 'body', 'title' => CONTACTUS_BODY, 'on' => $options['displaybody'],'type'=>'Textarea','width'=>12,'validate' => [['method' => 'notempty']]),
		);
		
		//Add initial fields value if no POST is present...
		$empty = false;
		$missinglist = array();
		if (!isset($request['postpresent'])){
			foreach($fields as $k => $v){
				switch($k) {
					case 'email' : {
						if ($service->get('User')->isLogged()) $fields[$k]['value'] = $service->get('User')->get('email');
					}break;
				}
			}
		}else {
			//Add fields values if post is present...
			//And scan for missing values...
			foreach($fields as $k => $v){
				if ($fields[$k]['on'] == 1) {
					if (isset($request[$k]) && !empty($request[$k])) {
						$fields[$k]['value'] = strip_tags($request[$k]);
					}else {
						$empty = true;
						$missinglist[] = $v;
					}
				}
			}
		}
		
		//Render form if no post present or there is missing/blank required fields...
		$service->get('Theme')->setTitle($this->data->getVar('content_title'));			
		$form = new Form(URL.$url['language'].'/'.$url['path'],'contactform',/*$this->data->getVar('content_title')*/'','POST');
		$form->setVar('layout','vertical');			
		$form->setVar('intro',$this->data->getVar('content_content'));

		//Send the form by mail if there is no missing fields and post is present.
		if (isset($request['postpresent']) && $empty == false) {
			$service->get('Ressource')->get('core/protocol/email');
			$email = new Email();
			$email->html(true);
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email->from($request['email'],$request['name']);
            }
            else {
                $email->from($service->get('Setting')->get('adminmail'),'Admin');
            }
			$to = $service->get('Setting')->get('adminmail');
			$email->to($to);
			//$email->to($service->get('Setting')->get('contactus_email'));
			$email->subject((isset($request['subject']))?$request['subject']:$this->data->getVar('content_title'));
			$body = '';
			foreach($fields as $v) {
				if ($v['on'] == 1) {
					$val = $request[$v['name']];
					if (is_array($val)) $val = implode("<br>",$val);
					if (strtolower($v['type']) == 'textarea' || strtolower($v['type']) == 'htmleditor') {
						$body .= "<br>".$v['title'].": <br>".$val."<br>";
					}else {
						$body .= $v['title'].": ".$val."<br>";
					}
					$body .= '<br>';
				}
			}
			$ip = "\n\n<br><br>".CONTACTUS_FORMWIDGET_FROMIP.'<a href="http://whatismyipaddress.com/ip/'.$_SERVER['REMOTE_ADDR'].'">'.$_SERVER['REMOTE_ADDR']."</a>\n";
			$body .= $ip."<br>".CONTACTUS_CLIENTLANG.': '.strtoupper($service->get('Language')->getCode())."\n";
			
			$email->body($body);
			$res = $email->send();
			$form->setTitle(CONTACTUS_TITLE);
			$form->setVar('intro',(($res)?CONTACTUS_SENDCONFIRM:CONTACTUS_SENDFAILED));
            if(!$res) {
                mail($to,'Contact us: cannot send message',"Cannot send the following message: \n\n".$body."\n\n Reason : ".$email->error());
            }
			return '<div class="row"><div class="cell col-sm-12">'.$form->render().'</div></div>';
		}else {
			$form->add(new HiddenFormField('postpresent','1'));
			foreach ($fields as $k => $v){
				if ($v['on'] == 1) {
					$name = $v['name'];
					$value = (isset($request[$v['name']]))?$request[$v['name']]:$v['value'];
					$params = array();
					if (isset($v['options'])) $params['options'] = $v['options'];
					if (isset($v['title'])) $params['title'] = $v['title'];
					if (isset($v['width'])) $params['width'] = $v['width'];
					if (isset($v['validate'])) $params['validate'] = $v['validate'];
					$cls = ucfirst(((isset($request['h_'.$v['name']]))?'Hidden':$v['type'])).'FormField';
					$form->add(new $cls($name,$value,$params));
				}
			}
			$form->add(new SubmitFormField('submit','',array('title' => CONTACTUS_SEND)));

			//Set error type if there is one...
			if (isset($request['postpresent'])) {
				$form->setVar('error',array('type'=>'error','msg'=>CONTACTUS_NOTVALID));
			}						
		}
		return $form->render();
	}
}