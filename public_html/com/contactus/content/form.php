<?php
/**
 * System form content handler
 *
 * Handles form content type
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Content');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');

class FormContent extends Content{

	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'contactus',
			'type' => 'form',
			'title' => 'Formulaire',
			'description' => 'Ajoute un formulaire à votre site web.',
			'icon' => 'contactus',
			'saveoptions' => array(
				'email','confirmmsg','sendcopytoclient','subject'
			),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	/**
	 * Returns the edit form to modify a section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);

		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);

		$form->setVar('title',_EDIT.' '.$defobj->getVar('content_title'));
		$form->add(new HtmleditorFormField('content_content',$defobj->getVar('content_content'),array(
			'title'=>_HEADER,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'content_content')
		)));

		//Form creation code...
		$form->add(new ItemmanagerFormField('fields',$defobj->getVar('content_options')['fields'],array(
			'title'=>'Champs',
			'tab'=>'basic',
			'parent' => $defobj->getVar('content_objid'),
			'editlabel' => _EDIT,
			'deletelabel' => _DELETE,
			'duplicatelabel' => _DUPLICATE,
			'newlabel' => _NEW.' champ',
			'list' => URL.$service->get('Language')->getCode().'/ajax/admin/content/form/list',
			'edit' => URL.$service->get('Language')->getCode().'/ajax/admin/content/form/edit',
			'duplicate' => URL.$service->get('Language')->getCode().'/ajax/admin/content/form/duplicate',
			'save' => URL.$service->get('Language')->getCode().'/ajax/admin/content/form/save',
			'delete' => URL.$service->get('Language')->getCode().'/ajax/admin/content/form/delete',
			'move' => URL.$service->get('Language')->getCode().'/ajax/admin/content/form/move',
		)));
		$form->add(new TextFormField('email',$defobj->getVar('content_options')['email'],array(
			'title'=>'Adresse courriel à laquelle envoyer le formulaire',
			'length'=>100,
			'tab'=>'basic'
		)));
		$form->add(new HtmleditorFormField('confirmmsg',$defobj->getVar('content_options')['confirmmsg'],array(
			'title'=>'Message de confirmation de l\'envoi',
			'length'=>100,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'content_options','confirmmsg')
		)));
		$form->add(new YesnoFormField('sendcopytoclient',$defobj->getVar('data_data')['sendcopytoclient'],array(
			'title'=>'Envoyer une copie du message au client?',
			'length'=>1,
			'tab'=>'basic'
		)));
		$form->add(new TextFormField('subject',$defobj->getVar('content_options')['subject'],array(
			'title'=>'Sujet du mail',
			'length'=>100,
			'tab'=>'basic'
		)));
		$form->add(new CheckboxFormField('content_draft',$defobj->getVar('content_draft'),array(
			'title'=>_DRAFT,
			'length'=>1,
			'tab'=>'basic',
			'options' => array(array('title'=>'','value'=>1))
		)));
		return $form;
	}

	/**
	 * Deletes content from other tables than section & content tables
	 */
	public function delete(){

	}

	/**
	 * Renders the section visual elements
	 */
	public function view($exerptonly=false){
		if ($exerptonly) $this->data->getVar('content_exerpt');

		global $service;
		$service->get('Ressource')->get('core/data');
		$service->get('Ressource')->get('core/display/form');
		$service->get('Ressource')->get('core/display/form/field');

		$url = $service->get('Url')->get();
		$request = $service->get('Request')->get();
		$store = new DataStore();

		// 1. Load all form elements
		$crit = new CriteriaCompo(new Criteria('data_type','formitem'));
		$crit->add(new Criteria('data_parent',$this->data->getVar('content_objid')));
		$crit->setSort('data_position');
		$fields = $store->get($crit);

		if (isset($request['op']) && $request['op'] == 'send'){
			$body = 'Voici les informations entrées dans le formulaire "'.$this->data->getVar('content_title').'" depuis le site '.$service->get('Setting')->get('sitename')."\n\n\n";
			$sourceemail = '';
			$seed = chr(rand(0,25)+65).rand(0,10000).chr(rand(0,25)+65).' ';
			$subject = $seed.'';
			//2. Setup form...
			$form = new Form(URL.$url['path'],'form','','POST');
			$form->setVar('intro',$this->data->getVar('content_options')['confirmmsg']);

			//3. Get form fields values from the form...
			foreach ($fields as $k => $v){
				$opt = $v->getVar('data_data');
				if ($opt['fieldtype'] != 'button' && $opt['fieldtype'] != 'submit') {
					if ($opt['fieldtype'] == 'textarea' || $opt['fieldtype'] == 'htmleditor') $body .= "\n";
					$body .= $v->getVar('data_title').': ';
				}

				if ($opt['issourceemail'] == 1) $sourceemail = $request['formfield'.$k];
				if ($opt['issubject'] == 1) $subject = $request['formfield'.$k];
				if ($opt['fieldtype'] == 'textarea' || $opt['fieldtype'] == 'htmleditor') {
					$body .= "\n";
				}
				//Parse options...
				if (trim($opt['options']) != '' && is_array($request['formfield'.$k])) {
					$body .= str_replace("\n\n","\n","\n&#10004;".implode("\n&#10004; ",$request['formfield'.$k])."\n");
				}else {
					if ($opt['fieldtype'] == 'fileupload' || $opt['fieldtype'] == 'imageupload') {
						$body .= '<a href="'.URL.$request['formfield'.$k].'">Télécharger le ficher</a>';
					}else {
						$body .= $request['formfield'.$k]."\n";
					}
				}
				if ($opt['fieldtype'] == 'textarea' || $opt['fieldtype'] == 'htmleditor') {
					$body .= "\n";
				}
			}
			$body .= "\n\n<hr><br>Envoyé depuis cette adresse IP: ".$_SERVER['REMOTE_ADDR']."\n";
			$body .= "Langue utilisée à l'envoi: ".strtoupper($service->get('Language')->getCode())."\n";

			$service->get('Ressource')->get('core/protocol/email');
			$email = new Email();
			$email->html(true);

			$to = $this->data->getVar('content_options')['email'];
			if ($to == '') $to = $service->get('Setting')->get('adminmail');
			$email->to($to);

			if ($subject != '') $subject = $this->data->getVar('content_options')['subject'].' ('.$subject.')';
			else $subject = $this->data->getVar('content_options')['subject'];
			$email->subject($seed.$subject);

			$tpl = $service->get('Theme')->getEmailTemplate();
			$tpl->assign('content',nl2br($body));
			$tpl->assign('title',$subject);
			$email->body($tpl->render());

			$email->from($sourceemail,$sourceemail);
			$res = $email->send();

			//Send a copy to the client if checked....
			if ($this->data->getVar('content_options')['sendcopytoclient'] == 1 && $sourceemail != '') {
				$body .= "\n\n\n<b>Pourquoi ai-je reçu ce courriel ?</b>\n\nVous recevez ce courriel car un usager à rempli le formulaire intitulé \"".$this->data->getVar('content_title').'\" depuis le site de '.$service->get('Setting')->get('sitename').". Si vous n'êtes pas l'auteur de cette action, veuillez communiquer avec nous à ".$this->data->getVar('content_options')['email'].".";
				$tpl->assign('content',nl2br($body));
				$email = new Email();
				$email->html(true);
				$email->to($sourceemail);
				$email->subject($this->data->getVar('content_title'));
				$email->from($this->data->getVar('content_options')['email'],$this->data->getVar('content_options')['email']);
				$email->body($tpl->render());
				$email->send();
			}

			if ($res) $form->setVar('intro',CONTACTUS_SENDCONFIRM);
			else $form->setVar(CONTACTUS_SENDFAILED);
		} else {
			//2. Setup form...
			$form = new Form(URL.$url['path'],'form','','POST');
			$form->setVar('intro',$this->data->getVar('content_content'));
			$form->add(new HiddenFormField('op','send'));

			//3. Add form fields to the form...
			foreach ($fields as $k => $v){
				$opt = $v->getVar('data_data');

				//Parse options...
				if (trim($opt['options']) != '') {
					$options = array();
					$opt['options'] = explode("\n",$opt['options']);
					foreach ($opt['options'] as $ko => $vo) {
						$tmpo = explode('|',$vo);
						switch (count($tmpo)) {
							case '1' : $options[] = array('title' => $tmpo[0], 'value' => $tmpo[0]); break;
							case '2' : $options[] = array('title' => $tmpo[1], 'value' => $tmpo[0]); break;
							case '3' : $options[] = array('title' => $tmpo[1], 'value' => $tmpo[0], 'help' => $tmpo[2]); break;
						}
					}
				}
				$cls = ucfirst(strtolower($opt['fieldtype'])).'FormField';
				$form->add(new $cls('formfield'.$k,$opt['value'],array(
					'title' => $v->getVar('data_title'),
					'placeholder' => trim($opt['placeholder']),
					'options' => $options,
					'showhelponclick' => $opt['showhelponclick'],
					'help' => $opt['help']
				)));
			}
		}
		return $form->render();
	}

	public function ajax($op=''){
		global $service;
		$service->get('Ressource')->get('core/data');
		$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
		$service->get('Ressource')->get('core/display/form');
		$service->get('Ressource')->get('core/display/form/field');

		$url = $service->get('Url')->get();
		$store = new DataStore();
		$store->setOption('ignorelangs',true);
		$request = $service->get('Request')->get();
		$datatype = 'formitem';
		$baseurl = '/'.$service->get('Language')->getCode().'/ajax/admin/content/form/';
		switch($url['path']) {
			/**
			 * Form elements management
			 */
			case $baseurl.'list' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$store->setOption('ignorelangs',false);
				$crit = new CriteriaCompo(new Criteria('data_type',$datatype));
				if ($request['parent'] > 0) $crit->add(new Criteria('data_parent',$request['parent']));
				$crit->setSort('data_position');
				$obj = $store->get($crit);
				$response = array();
				foreach($obj as $v) {
					$response[] = array('title'=> $v->getVar('data_title'),'id' => $v->getVar('data_objid'));
				}
				echo json_encode(array('success'=>1,'data'=>$response));
				die();
			}break;
			case $baseurl.'move' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$store->setOption('ignorelangs',true);
				$obj = $store->getByObjId($request['id']);
				$offset = intval($request['offset']);
				if ($offset != 1 && $offset != -1) {
					$resp = false;
				}else {
					$oldpos = $obj[0]->getVar('data_position');
					$newpos = $obj[0]->getVar('data_position')+$offset;
					$parent = $obj[0]->getVar('data_parent');
					$crit = new CriteriaCompo(new Criteria('data_type',$datatype));
					$crit->add(new Criteria('data_parent',$parent));
					$crit->add(new Criteria('data_position',$newpos));
					$obj2 = $store->get($crit);
					if (count($obj2) != 0) {
						foreach ($obj as $k => $v){
							$obj[$k]->setVar('data_position',$newpos);
						}
						foreach ($obj2 as $k => $v){
							$obj2[$k]->setVar('data_position',$oldpos);
						}
						$store->save($obj);
						$store->save($obj2);
					}
				}
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;
			case $baseurl.'duplicate' : {
				if ($request['id']) {
					$itemsparent = $request['id'];
					$obj = $store->getByObjId($request['id']);
					$next = $store->getNext();
					$nextpos = $store->getNext('data_position',new Criteria('data_type',$datatype));
					foreach ($obj as $k => $v) {
						$obj[$k]->setVar('data_id',0);
						$obj[$k]->setVar('data_parent',$request['parent']);
						$obj[$k]->setVar('data_objid',$next);
						$obj[$k]->setVar('data_position',$nextpos);
					}
					$store->save($obj);
					/*
					$request['id'] = $next;

					//Duplicate all product model items too..
					$crit = new CriteriaCompo(new Criteria('data_parent',$itemsparent));
					$crit->add(new Criteria('data_type','productmodelitem'));
					$crit->setOrder('ASC');
					$crit->setSort('data_position');
					$obj = $store->get($crit);
					$objids = array();
					foreach ($obj as $k => $v){
						$obj[$k]->setVar('data_id',0);
						$obj[$k]->setVar('data_parent',$next);
						$objids[] = $v->getVar('data_objid');
					}
					$next++;
					foreach ($objids as $ko => $vo) {
						foreach($obj as $k => $v ){
							if ($v->getVar('data_objid') == $vo) {
								$obj[$k]->setVar('data_objid',$next);
							}
						}
						$next++;
					}
					$store->save($obj);
					*/
				}
			}
			case $baseurl.'edit' : {
				@header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$langs = $service->get('Language')->getCodes();
				$form = new Form('', 'fieldsform', 'Edit Fields', $method='POST');
				$form->setVar('formtag',false);
				if ($request['id']) {
					$obj = $store->getByObjId($request['id']);
				}else {
					$obj = $store->createMultilingual();
					$max = $store->getNext('data_position',new Criteria('data_type',$datatype));
					foreach($obj as $k => $v) {
						$obj[$k]->setVar('data_type',$datatype);
						$obj[$k]->setVar('data_parent',$request['parent']);
						$obj[$k]->setVar('data_position',$max);
					}
					$store->save($obj);
				}
				$defobj = $store->getDefaultObj($obj);
				$id = $defobj->getVar('data_objid');
				$title = $defobj->getVar('data_title');
				if ($title == '' && $request['id'] == 0) $title = _NEW.' '.'str: champ';
				$data = $defobj->getVar('data_data');
				$form->add(new HiddenFormField('data_id',$defobj->getVar('data_id'),array(
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_id')
				)));
				$form->add(new HiddenFormField('data_type',$datatype));
				$form->add(new HiddenFormField('data_parent',$request['parent']));
				$form->add(new HiddenFormField('data_objid',$id,array(
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_objid')
				)));

				$form->add(new SelectFormField('fieldtype',$defobj->getVar('data_data')['fieldtype'],array(
					'title'=>'Type de champ',
					'options' => array(
						array('title' => CONTACTUS_FORMWIDGET_FIELD_TEXT, 'value' => 'text'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_CHECKBOX, 'value' => 'checkbox'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_DATE, 'value' => 'datepicker'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_DIMENSIONS, 'value' => 'imagesize'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_FILE, 'value' => 'fileupload'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_IMAGE, 'value' => 'imageupload'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_HTMLEDITOR, 'value' => 'htmleditor'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_LABEL, 'value' => 'label'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_PWD, 'value' => 'password'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_RADIO, 'value' => 'radio'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_SELECT, 'value' => 'select'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_LANGSELECT, 'value' => 'selectlanguage'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_MULTISELECT, 'value' => 'selectmultiple'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_GROUPSELECT, 'value' => 'selectusergroup'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_SUBMITBTN, 'value' => 'submit'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_TEXTAREA, 'value' => 'textarea'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_YESNO, 'value' => 'yesno'),
						array('title' => CONTACTUS_FORMWIDGET_FIELD_HIDDEN, 'value' => 'hidden')
					)
				)));
				$form->add(new TextFormField('data_title',$defobj->getVar('data_title'),array(
					'title'=>_TITLE,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_title')
				)));
				$form->add(new TextFormField('placeholder',$defobj->getVar('data_data')['placeholder'],array(
					'title'=>'Texte indicatif (placeholder)',
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','placeholder')
				)));
				$form->add(new TextFormField('value',$defobj->getVar('data_data')['value'],array(
					'title'=>'Valeur par défaut',
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','value')
				)));
				$form->add(new TextareaFormField('options',$defobj->getVar('data_data')['options'],array(
					'title'=>'Options',
					'length'=>255,
					'help' => 'Une option par ligne en respectant les formats requis: valeur|titre|help ou valeur|titre ou titre seulement.',
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','options')
				)));
				$form->add(new YesnoFormField('showhelponclick',$defobj->getVar('data_data')['showhelponclick'],array(
					'title'=>'Afficher l\'aide lors de la sélection des options',
					'length'=>255,
				)));
				$form->add(new YesnoFormField('issourceemail',$defobj->getVar('data_data')['issourceemail'],array(
					'title'=>'Ce champ représente l\'adresse courriel à laquelle envoyer une copie du message'
				)));
				$form->add(new YesnoFormField('issubject',$defobj->getVar('data_data')['issubject'],array(
					'title'=>'Ce champ représente le sujet du courriel'
				)));
				$form->add(new TextareaFormField('help',$defobj->getVar('data_data')['help'],array(
					'title'=>'Texte d\'aide du champ',
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','help')
				)));
				$form->addSaveButton('save','Enregistrer le champ');
				$form->setVar('title',$title);
				echo json_encode(array('success'=>1,'form'=>$form->renderArray()));
				die();
			}break;
			case $baseurl.'save' : {
				header('Content-type: application/json');
				$saveRequest = new RequestToObject('data');
				$saveRequest->addOptions(array(
					'fieldtype','placeholder','value', 'options', 'showhelponclick', 'help','issourceemail','issubject'
				),'data_data');
				$resp = $saveRequest->save();
				$resp = false;
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;
			case $baseurl.'delete' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$langs = $service->get('Language')->getCodes();
				if ($request['id']) {
					$resp = $store->delete($request['id']);
				}else {
					$resp = false;
				}
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;
			default : break;
		}
	}
}
