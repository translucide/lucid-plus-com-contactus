<?php
/**
 * Contact Us route view
 *
 * Default view loaded on contact us page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view');

class ContactusView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		return $this->data['form'];
	}
}
?>