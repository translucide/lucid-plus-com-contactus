<?php
global $service;
$service->get('Ressource')->get('com/contactus/lang/'.$service->get('Language')->getCode().'/settings');
$service->get('EventHandler')->on('settingmanager.onBeforeLoad',
	function($e,$p){
		global $service;
		$cid = 1;
		$catStore = new SettingCategoryStore();
		$catStore ->setOption('ignorelangs',true);
		$settingStore = new SettingStore();
		$settingStore->setOption('ignorelangs',true);
		$cat = $catStore->get(new Criteria('settingcategory_name','CONTACTUS_SETTINGCATEGORY'));
		if (!is_array($cat) || !count($cat)) {
			$obj = $catStore->create();
			$obj->setVar('settingcategory_name','CONTACTUS_SETTINGCATEGORY');
			$obj->setVar('settingcategory_language',0);
			$catStore->save($obj);
			$cat = $obj;
			$cid = $cat->getVar('settingcategory_objid');

			//Add new setting elements here...
			$obj = $settingStore->createMultilingual();
			foreach ($obj as $k => $v) {
				$obj[$k]->setVar('setting_cid',$cid);
				$obj[$k]->setVar('setting_name','contactus_address');
				$obj[$k]->setVar('setting_value','');
				$obj[$k]->setVar('setting_type','textarea');
				$obj[$k]->setVar('setting_options','');
				$obj[$k]->setVar('setting_title','CONTACTUS_SETTING_ADDRESS');
				$obj[$k]->setVar('setting_description','CONTACTUS_SETTING_ADDRESS_DESC');			
			}
			$settingStore->save($obj);

			//Add new setting elements here...
			$obj = $settingStore->create();
			$obj->setVar('setting_cid',$cid);
			$obj->setVar('setting_name','contactus_phone');
			$obj->setVar('setting_value','');
			$obj->setVar('setting_type','textarea');
			$obj->setVar('setting_options','');
			$obj->setVar('setting_title','CONTACTUS_SETTING_PHONE');
			$obj->setVar('setting_description','CONTACTUS_SETTING_PHONE_DESC');
			$obj->setVar('setting_language',0);
			$settingStore->save($obj);

			//Add new setting elements here...
			$obj = $settingStore->create();
			$obj->setVar('setting_cid',$cid);
			$obj->setVar('setting_name','contactus_fax');
			$obj->setVar('setting_value','');
			$obj->setVar('setting_type','text');
			$obj->setVar('setting_options','');
			$obj->setVar('setting_title','CONTACTUS_SETTING_FAX');
			$obj->setVar('setting_description','CONTACTUS_SETTING_FAX_DESC');
			$obj->setVar('setting_language',0);
			$settingStore->save($obj);

			//Add new setting elements here...
			$obj = $settingStore->create();
			$obj->setVar('setting_cid',$cid);
			$obj->setVar('setting_name','contactus_email');
			$obj->setVar('setting_value','');
			$obj->setVar('setting_type','text');
			$obj->setVar('setting_options','');
			$obj->setVar('setting_title','CONTACTUS_SETTING_EMAIL');
			$obj->setVar('setting_description','CONTACTUS_SETTING_EMAIL_DESC');
			$obj->setVar('setting_language',0);
			$settingStore->save($obj);
		}
	}
);
?>